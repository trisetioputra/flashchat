import 'package:flash_chat/screens/login_screen.dart';
import 'package:flash_chat/screens/registration_screen.dart';
import 'package:flutter/material.dart';
import 'package:flash_chat/components/NavigationButton.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class WelcomeScreen extends StatefulWidget {
  static String id = 'welcome_screen';
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  //single ticker karena animasi yang dipakai hanya 1
  AnimationController controller;
  Animation animation;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
      // upperBound: 1,
      // lowerBound: 0,
      duration: Duration(seconds: 2), // akan nge tick dari 0 - 1 dalam x seconds
      vsync: this,
    );
    //bound curved harus 0-1
    controller.forward(); //kalo mau mundur tickernya jadi reverse aja
    animation=ColorTween(begin: Colors.blueGrey,end: Colors.white).animate(controller);

    //animation=CurvedAnimation(parent: controller, curve: Curves.decelerate); //jenis curve liat di dokumentasi
    // animation.addStatusListener((status) {
    //   //bakal besar -> kecil -> besar dst.
    //   if(status==AnimationStatus.completed){
    //     controller.reverse(from:1.0);
    //   }
    //   else if(status==AnimationStatus.dismissed){
    //     controller.forward();
    //   }
    // });
    controller.addListener(() {
      setState(() {

      });
      //print(controller.value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Hero(
                  tag: 'logo',
                  child: Container(
                    child: Image.asset('images/logo.png'),
                    height: 60.0,//animation.value *100,
                  ),
                ),
                TypewriterAnimatedTextKit(
                  text:['Flash Chat'],
                  textStyle: TextStyle(
                    fontSize: 45.0,
                    fontWeight: FontWeight.w900,
                  ),
                  speed: const Duration(milliseconds: 200),

                ),
              ],
            ),
            SizedBox(
              height: 48.0,
            ),
            NavigationButton(
              text: "Log In",
              onPressed: () {
                Navigator.pushNamed(context, LoginScreen.id);
              },
              color: Colors.lightBlueAccent,
            ),
            NavigationButton(
              text: "Register",
              onPressed: () {
                Navigator.pushNamed(context, RegistrationScreen.id);
              },
              color: Colors.blueAccent,
            )
          ],
        ),
      ),
    );
  }
}


